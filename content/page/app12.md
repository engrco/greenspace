---
title: Scientific History and History of Math, Culture and Science
subtitle: Experimental Design, Accelerated Learning and Continuous Improvment
comments: false
---


If we really want to make History, we are going to need to try a lot harder to try making History come out better, ***predictably***.  It is terribly easy now to simulate ... to just build models and programmatically simulate Reality ... we should use simulations, but we must understand the flaws in our simulations. 

Simulations will always just be simulations ... but building simulations can be extremely useful, in forcing us to think about modeling dynamics and change of different parameters ... but only if [the simulated outcomes of these progressions can be experimentally substantiated](https://arxiv.org/pdf/2110.00691.pdf) ... without the *real world* experimental validation of the theoretical simulations, the simulations are nothing but fantasy ... fantasy has merit, of course, but it should not be used for anything of predictive nature.

We are going to need REAL world test, evaluation and correction ... it's probably somewhat like testing in production, where we can always pull the plug on the canary development and switch back to what works ... maybe we should use something like orthogonal arrays to try different Production environments ... but, really, this business of making history has to stop being so damned violent.