---
title: The Process is about improving the literacy of local Nature
subtitle: Stewardship provides returns far in excess to what people put into it
comments: false
---

It's very simple ... be a better steward of small, local things.