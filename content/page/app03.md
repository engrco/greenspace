---
title: Physics, Energetics, Electromagnetics, Geoscience
subtitle: The REALLY Basic Science of Energy, Matter and Time
bigimg: [{src: "https://upload.wikimedia.org/wikipedia/commons/9/96/Diamond_animation.gif", desc: "Molecular Physics"}, {src: "https://upload.wikimedia.org/wikipedia/commons/f/f3/Membranes_perforated_at_two_strutting_angles.gif", desc: "Condensed Matter Physics"}, {src: "https://upload.wikimedia.org/wikipedia/commons/b/bc/Bosons-Hadrons-Fermions-RGB.svg", desc: "Particle Physics"}, {src: "https://upload.wikimedia.org/wikipedia/commons/4/4b/Lightning3.jpg", desc: "Plasma Physics"}, {src: "https://upload.wikimedia.org/wikipedia/commons/5/54/Rayleigh-Taylor_instability.jpg", desc: "Computational Physics"}]
comments: false
---


The bar is set impossibly high for this app, since any app needs to be a better use of someone's time than the [legendary Feynman lectures on Physics](https://feynmanlectures.caltech.edu/) ... thus, this app, like ALL of the apps is not meant to replace what is there, but rather the point of the app is to ***complement*** what is there ... moreover, ***architecting and building*** a better open source app to ***teach*** Physics problems is learning exercise in itself.

It is ***relaxing*** to *just read* about Physics, starting with the [Wikipedia Physic portal](https://en.wikipedia.org/wiki/Portal:Physics), ie *just to be dangerous* ... an encyclopedia can illustrate ideas and help us with the definitions and jargon -- yes, it really *only* gives us the keywords and basic definitions to help us connect the dots when we get done to DOING problems ... BUT *only* is enough to lay the foundation before we get started.

With a very few exceptions, VERY LITTLE has really changed sinced when Richard Feynman gave these lectures because the lectures were not so much about Physics as they were primarily about a way of thinking, approaching a problem, constructing a mental framework to solve problems and perhaps conduct experiments to prove some hypothesis from our theoretical framework that we have deduces ... the Feynman approach to Physics is still relevant, but as Feynman said in his Preface to the printed version of these lectures, there's still room for students to improve their understanding of the principles and Feynman gave us a clue about how students can help themselves, *"One way we could help the students more would be by putting more hard work into developing a set of problems which would elucidate some of the ideas in the lectures. Problems give a good opportunity to fill out the material of the lectures and make more realistic, more complete, and more settled in the mind the ideas that have been exposed."*


***You cannot fix STUPID ... STUPID exists; you need to distance yourself from STUPID and never encourage it ... but when STUPID starts following you, you must ELIMINATE what is feeding it from inside your life.  Do not try to fix STUPID.***

