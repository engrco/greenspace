---
title: Math, Statistics, Logic
subtitle: More than 99% of problem solving is in how we set up the problem
comments: false
---

Math is beautiful ... and maybe it's beautiful because it helps us explain or model the progressions that shape the universe ... perhaps appreciation of Math is an acquired taste, but Math is arguably more beautiful than art or music ... the beauty of this existence is that there's no reason why we can't enjoy all three ... but Math is pure magic!

Stupid pet tricks are for laughing AT people who waste their time and their pet's time teaching their pets to do idiotic tricks -- a lot of people never understood that the point of a televised gossip fest was to point out the idiocy of gossiping and showing off idiocy. 

MOST of what passes for data science and data visualization today is very much the equivalent of a stupid pet trick -- the world is fully of misleading and compoundingly stupid data tricks. The data is tortured to produce something like a conclusion that supports a particularly biased point of view -- it's like teaching Arnold Ziffel to enjoy watching college football on TV in order to prove that everyone should enjoy watch college football on TV. 

The point of building this open source app is to help you to:

1) Radically challenge your assumptions
2) Study mathematics for its beauty alone
3) Study statistics, only to find its flaws
4) Study logic for its tendencies to produce arrogance


Obviously, the bar is set impossibly high for this app or set of apps, since any app needs to be a better use of someone's time than the [Mathematics Portal on Wikipedia](https://en.wikipedia.org/wiki/Portal:Mathematics#WikiProjects) and that means that it must also be a better use of time than EVERYWHERE that you can possible go from there, such as the current arXiv pre-print publications and ConnectedPapers.

Therefore, this app, like ALL of the apps is not meant to replace what is there, but rather the point of the app is to ***complement***, build upon, use and even find new connections that are out there ... moreover, ***architectig and building*** a better open source app to teach Mathematics [and maybe Statistics and Data Science] problems is a bit of Mathematics learning exercise in itself.

Of course Mathematics is still a creative, evolving field ... but don't worry -- VERY LITTLE of what is necessary to build this app will change while you study Mathematics and Statistics ... there's still plenty of room for students to improve their understanding of Mathematics principles; students can best teach themselves and help others by teaching themselves in the building of an open source problems app. 


***You cannot fix STUPID ... STUPID exists; you need to distance yourself from STUPID and never encourage hate or spread hysteria or chaos ... but when STUPID starts following you, you must ELIMINATE what is feeding the STUPID from inside your life.  Avoid hysteria;do not try to fix hate.***

