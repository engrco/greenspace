---
title: Discipline, Mind, Self-Control
subtitle: Achieving More Effective Compassion
comments: false
---

The POINT of this first app is Discipline, Mind Training, and moving beyond Self-Control to Self-Improvement ... there are days when everything doesn't go our way, but in general, we get out of Life much more than we ever put into it ... we should aim to be **both** appreciators of natural law and order or perfect practicing [Stoics](https://en.wikipedia.org/wiki/Stoicism) and appreciators of beauty and pleasure or perfect practicing [Epicureans](https://en.wikipedia.org/wiki/Epicureanism) **before** we ever blame or gossip about others. The fact is ... **whining is always caused by a failure of imagination or just not trying hard enough.** Viktor Frankl showed us why logotherapy trumps psychoanalysis; it is necessary to sieze the responsibility to find meaning in Life, regardless of the circumstances. 

We cannot find meaning when our consciousness is clouded by alcohol or escapism, ie just watching tv, film or sporting events. Finding meaning is a deliberate act. With meditative awareness or constantly being in state of prayerful wakefulness we can practice tonglen, focusing on the breath like a built-in metronome to constantly take in difficulties and exhale optimism. Constancy of purpose or the discipline of simple compounding to continual improvement makes things ***better OVER TIME*** ... no problem is ever going to be fixed in one dramatic, revolutionary sweep ... making things ***better OVER TIME*** requires DECADES of diligent, breath-by-breath focus on compounded improvement.

The mental model for us in thinking about this should be something like a CONTINUOUSLY COMPOUNDED one-tenth of one-percent improvement every day ... for each $9.99 of effort we spend, we *buy smarter* to get back the full $10 -- for exammple, for the same amount of food/shelter, we should be 1/10th of one percent healthier tomorrow ... for each $10 of output we produce, we should aim to get $10.01 of value -- obviously, we will probably need to change jobs and become OWNERS in order realize the benefit of this increase in value.  

The point of the slogan "***Discipline Equals Freedom*** is achieving **RELENTLESS constancy of purpose** in everything that is under our control and completely quitting on everything that is beyond our control.

Discipline is something that is built, breath-by-breath ... EVERY SINGLE DAY, ALL YEAR LONG, YEAR AFTER YEAR ... the discipline of ***IMPROVEMENT*** is relentless... 1/10th of one percent improvement in inputs AND 1/10th of one percent improvement in outputs ... CONSTANCY OF PURPOSE and these two continually compounded improvements will result in a 150X gain over ten years.  

## At first, improvement is MOSTLY about what one takes away ... affluence and desire kills the mind first.

*Whether it's tits, tires or sheetrock, the desire for things is going to cost us 1000X, 100X, 10X more than we imagine the highest cost might be.*

Ever since humans started acquiring more than what was necessary to surive well, humans have been plagued by the crippling disease of envy, jealousy and pointless longing for more baggage ... this was fairly obvious to ancient mystics and anyone who distanced themselves from the mass of humankind afflicted with this disease. Meditative awareness and prayer begins with translating, discussing and elaborating upon what can gleaned from materials like [the ancient texts on Wisdom](https://read.84000.co/translation/toh11.html) ... but this app is intended to complement, not replace, the time we spend studying these texts.

Complementation is the foundation of every app our set of twelve apps because the objective is about the architecture of HOW we learn, HOW we educate, HOW we do SCIENCE.  The objective is never about WHAT we might learn or WHAT people might believe. It's about HOW people get there, not WHAT they see when they are there.  Science is a journey, not a destination; it REQUIRES questioning, skepticism, humility ... ***the SCIENCE is NEVER settled!***  

Apps are about learning how we learn, helping ourselves continually improve.  The ***one who learns the most in any classroom is the instructor*** which is exactly why helping others, even if it means learning the hard way ON THE FLY, teaching ourselves and testing our knowledge in REAL applied settings is best way to learn another facet of any kind of knowledge.   

We use apps to help ourselves change the habits of being ourselves ... the discipline that we further with the use of our app helps us to  institutionalize and test our skills -- learning about how to build, monitor and improve educational repositories of learning infrastructure-as-code that individuals use for their own gathering of intell.  

Building apps requires a multi-facted skill stack ... but dogfooding every aspect of our self-improvement process is how we do SCIENCE.

### The best way to LEARN is ***to do***, **to EXERCISE**

We want to **LEARN** how to integrate data APIs into an open source tool that MIGHT add value -- if that weren't already clear, let's be even more CRYSTAL EFFING CLEAR. The point of doing this is about learning ... and that includes learning how to learn ***by doing*** AND to help others learn how to learn and teach others to learn.  As a practical exercise toward that end, all of us will LEARN how to use and integrate data APIs and getting the practical experience with what kinds of things go wrong. This is emphasized redundantly so that everyone involved remembers what the objective is in LEARNING and understands the very limited, primarily educational scope of this LEARNING exercise.  

To re-emphasize ... the whole POINT of this app is learning about learning, that makes it being about taking control of EDUCATION.  PRACTICAL hands-on, actually-valuable, lifelong education!  Education all the way down -- education for yourself; education for others you care about. Learning about how to do education ***BETTER.*** If this exercise is about disrupting anything, it's about disrupting the completely worthless education system, especially the diploma grifters who claim to be in the education business, but are actually scamming parents/students into to paying ginormous tuitions in order to make a kid APPEAR employable.  

### Work Breakdown Schedule

The FIRST lesson to be learned is that development is not, any longer, about coding something in one language with just one framework, just one data store.  The overall architecture will reflect the different levels of data and meta-data output driven by data inputs from a variety of sources, with a setup that scales easily but is optimized for analytic processing in a configurable notebook which, in turn, either serves as a lite weight CRM tool or offers the opportunities to configure the export to CRM tools.  The point of our lesson is that development is about fault-tolerant integration of inter-networked services, rather than the details of what those services do, ie there's more than enough to do in bringing all of the different services together -- we do not have to worry about the code that implements something like Kafka or MongoDB. 

![overall architecture](https://learning.oreilly.com/library/view/agile-data-science/9781491960103/assets/ags2_0202.png)

In the grand scheme ... we are interested in LEARNING how to develop or hack the tools that we use for gathering intell about the evolution of job situation or market for skilled services ... we want to learn how to do this BETTER, because INDEPENDENT people cannot afford to rely on friends or headhunters or job boards to get them a job.  The intell from the job market informs the INDEPENDENT person where to focus efforts.

The final objective of this particular app would be to gather all data from existing job boards ... it's JUST a learning exercise about building this tool.

We will assume that developer want a tool that yields a semi-analyzable dashboard which allows for "playing with the data" after it's delivered ... something like a Jupyter notebook or a web-based coding play ground, ie RATHER than just search results a la Google. Other revs after the initial working thing would naturally be more ambitious ... but the specific nature of what that might mean would be determined by getting a functional prototype working and using it for awhile.

The work is structured or broken up ... so that you get up ... walk around ... THINK ... DO NOT SIT!  **This is NOT about taking breaks -- this is about THINKING MORE CLEARLY ... THINKING MORE AGGRESSIVELY ... which people do when they are standing and walking around ... LEARN TO THINK ... stop imagining that you think better when you sit.  You might type better when you sit ... but in order to THINK -- you need to WALK and MOVE.  Get in the habit of THINKING -- STOP sitting!!!**

There are different parts to this exercise:

1)  Community, eg recruiting other devs and users to adopt the open source tool AND joining OTHER communities that provide important parts of the puzzle.
2)  Outputs, eg UX design, Figma, what would we imagine that users would want to do with the app
3)  Observability, ie, engineering the monitoring of everything in the use of the app, to improve/correct the value of everything
4)  Inputs, eg data APIs, CDNs and external data sources that will drive app
5)  Conversion, ie everything in the infrastructure to turn inputs into [initial] outputs, which a user "plays with" for their own final output

## Community

We start here because we want to start THINKING about getting others on board FIRST ... naturally, we will not be announcing anything until we have something to show ... but the FIRST thing that we will want to do is to see what is already out there ... that includes whether this is already a community of open source developers who are building something like our Job Postings Analysis Notebook app. 

## Outputs

After surveying the global lay of the land, we will turn our focus into exactly what we want the END product of our Job Postings Analysis Notebook app to look like ... and since our design will need to evolve over time, we will use Figma ... and that means Gitified version control of Figma in Gitlab.

## Inputs

This is where data APIs as well as CDNs and a potentially GIANT universe of job-related, career-related, skills-related external data sources come in ... beyond Indeed or StackExchange careers or LinkedIn, this also includes content providers like OReilly, Open Courseware or the [expanding world of different online courses/tutorials](https://www.simplilearn.com/resources-to-learn-data-science-online-article) ... this could be seen as the big WHY of WHY learn to do this, because it's always going to be a case of new data providers entering this space, ie competition from an explosion of others is why there is no good reason to re-invent what a company like Indeed already excels at; it's not just that Indeed does it, there are a million idiots out there who imagine that they can do it better than Indeed ***and maybe some actually can.***  

## Conversion

The components that we expect to use in our conversion are

* Events ... an event is a job posting that is listed on a job board and eventually logged in our app along with its features, timestamps, revisions ... probably something like JSON ... but these events are going to come from a variety of different job boards and data stores.
* Collectors ... involve the event streaming of job posting aggregators ... we might use Kafka serve as the foundation of our event-driven architectures or microservice, but there are [message broker alternatives to consider](https://www.slant.co/options/961/alternatives/~kafka-alternatives) ... so this is where we want to start looking HARD at [observativability engineering](https://learning.oreilly.com/library/view/observability-engineering/9781492076438/) and the [OVERALL view of cloud native architecture](https://learning.oreilly.com/library/view/design-patterns-for/9781492090700/)
* Bulk storage ... local filesystem cache-like storage, eg Hadoop Distributed File System (HDFS) and possibly larger stores such as AWS's S3 or some ultra-affordable online storage.  Latency will not be much of an issue for this application, ie it's not like trading stocks or getting current airline flight status [where latency can matter a lot as one is running throuh an airport] but we probably should assume some need to for design flexibility since that could be different for other users or different in the future.
* Distributed document stores ...  are multinode stores using document format, probably MongoDB.
* Minimalist web application ... Python/Flask and Python sklearn or xgboost. Other simple web frameworks in Node.js might make sense.  Web Assembly opening us some options that make a web app behave a look like an app running in a container.
* A modern browser using [Chromium](https://sites.google.com/a/chromium.org/dev/Home) ... possibly something for a VERY MINIMAL tablet or Chromebook or maybe even Amazon Fire tablet or Kindle simliar content delivery device [if Amazon gets its head out of its tail on the whole topic of browser technology] ... the point here is that some users might be using things that simply do not afford the FULL notebook -- but we will still need to make it abundantly clear that something like Chromium is a requirement OR ELSE.
* The FULL job postings analysis notebook, BEYOND the minimalist web app, is the FULL applications that the END users is going to need something like a CRM tool ... or perhaps that user will export the data from the analysis notebook into a CRM tool ... it's not about one-time search, it's about the user building either a sales funnel OR a continual sales flywheel, where the user comes back again and again to organizations that might need the user's expertise.

## Observability

This might appear to be left until last [because we can't discuss it at the same time as we discuss the above items]... but in Reality, Observability will be designed into the app from the start -- all code will be reviewed with an eye toward the execution of that code delivering data about customer usage to a dashboard that drives the improvement of the app ... in practice, that data and backend dashboard might never be "turned on" ... but the architecture and structure of the code will be engineered to offer observability.