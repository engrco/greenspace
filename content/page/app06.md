---
title: Linguistics, Computational Linguistics and Knowledge Connections
subtitle: The Science of asynchronous human thought and its patterns
comments: false
---

Linguistics itself is perhaps not a PRIMARY interest, but we have to care sharing thoughts with other humans, both those alive now, those who have come before us and those who will look to us. Connection in human thought and knowledge drives our interest in [Linguistics](https://en.wikipedia.org/wiki/Portal:Linguistics). Information technology in AI and deep learning cannot begin to really process ideas, conceptual connections, symbols, words, idioms, equations without linguistics to point the way in terms of patterns that humans care about .... of course, this interest in human patterns carries over into understanding History ... we learn things about the use of the body to commuicate when we do martial arts or gain a personal, direct felt understanding of any competitive physical activity ... the same applies to artistic expression or performance.