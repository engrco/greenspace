---
title: Chemistry
subtitle: The Intermediary Between Physics and Biology
comments: false
---



Chemistry is [the central science](https://en.wikipedia.org/wiki/The_central_science) because it connects the physical sciences with the life sciences and applied sciences such as medicine and engineering.

Obviously, the bar is set impossibly high for this app or set of apps, since any app needs to be a better use of someone's time than the [Chemistry Portal on Wikipedia](https://en.wikipedia.org/wiki/Portal:Chemistry) and that means that it must also be a better use of time than EVERYWHERE that you can possible go from there, such as the current ChemRxiv pre-print publications and ConnectedPapers.

Therefore, this app, like ALL of the apps is not meant to replace what is there, but rather the point of the app is to ***complement***, build upon, use and even find new connections that are out there ... moreover, ***architectig and building*** a better open source app to teach Chemistry problems [along with maybe Nanochemistry and [some modelling or simulation in the virtual realm](https://github.com/deepchem/deepchem)] is necessarily going involve bit of an exercise in learning Chemistry.
