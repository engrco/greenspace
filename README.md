## Voluntary local stewardship

Greenspace Group advocates increasing the level of VOLUNTARY stewardship practiced by citizens and small landholders in their local communities and greenspace environments.

## Stewrds build soil which improves local food security and help to more optimally sequester carbon